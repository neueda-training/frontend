import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-trade-edit',
  templateUrl: './trade-edit.component.html',
  styleUrls: ['./trade-edit.component.css']
})
export class TradeEditComponent implements OnInit {
  id = this.actRoute.snapshot.params['id'];
  tradeDetails: any = {};
  constructor(
    public restApi: RestApiService,
    public actRoute: ActivatedRoute,
    public router: Router
  ) { }


  ngOnInit(): void {
    this.restApi.getTrade(this.id).subscribe((data: {}) => {
      this.tradeDetails = data;
    })
  }

  updateTrade() {
    if(window.confirm('Are you sure, you want to update?')){
      this.restApi.updateTrade(this.id, this.tradeDetails).subscribe(data => {
        this.router.navigate(['/trade-list'])
      })
    }
  }

}
