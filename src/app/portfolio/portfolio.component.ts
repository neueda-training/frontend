import { Component, OnInit } from '@angular/core';
import { RestApiService } from '../shared/rest-api.service';
@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnInit {

  Portfolio: any = [];
  constructor(public restApi: RestApiService) { }

  ngOnInit(): void {
    this.loadPortfolio()
  }

  loadPortfolio(){
    return this.restApi.getAllPortfolio().subscribe((data: {}) => {
      this.Portfolio = data;
      console.log(this.Portfolio);
  })
}
}
