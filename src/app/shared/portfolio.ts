import { DatePipe } from "@angular/common";

export class Portfolio {
    constructor(){
        this.id=0;
        this.ticker="";
        this.avgBuyPrice=0;
        this.volume=0;
        this.avgSellPrice=0;
        this.stocksBought=0;
        this.stocksSold=0;
        this.position="";
    }
    id: number;
    ticker: string;
    avgBuyPrice: number;
    volume: number;
    avgSellPrice: number;
    position: string;
    stocksSold: number;
    stocksBought: number;
}