import { DatePipe } from "@angular/common";

export class Trade {
    constructor(){
        this.id=0;
        this.stockTicker="";
        this.price=0;
        this.volume=0;
        this.buyOrSell="";
        this.orderTime= new Date();
        this.statusCode=0;
    }
    id: number;
    stockTicker: string;
    price: number;
    volume: number;
    buyOrSell: string;
    orderTime: Date;
    statusCode: number;

}
