import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Trade } from '../shared/trade';
import { Observable, throwError } from 'rxjs';
import { retry, catchError ,map} from 'rxjs/operators';
import { Portfolio } from '../shared/portfolio';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RestApiService {
  apiURL = 'http://localhost:8080/api/trades/';
  portfolioURL = 'http://localhost:8080/portfolio/';

  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }  

  getAllPortfolio(): Observable<Portfolio>{
    return this.http.get<Portfolio>(this.portfolioURL)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API get() method 
  getAllTrades(): Observable<Trade> {
    return this.http.get<Trade>(this.apiURL)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API get() method 
  getTrade(id:any): Observable<Trade> {
    return this.http.get<Trade>(this.apiURL + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  

  // HttpClient API post() method 
  newTrade(trade:Trade): Observable<Trade> {
    return this.http.post<Trade>(this.apiURL + '', JSON.stringify(trade), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  } 

  // HttpClient API put() method
  updateTrade(id:number, trade:Trade): Observable<Trade> {
     return this.http.put<Trade>(this.apiURL + trade.id, JSON.stringify(trade), this.httpOptions)
     .pipe(
       retry(1),
       catchError(this.handleError)
     )
   }

  // HttpClient API delete() method 
  deleteTrade(id:number){
    return this.http.delete<Trade>(this.apiURL + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // Error handling 
  handleError(error:any) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }

}