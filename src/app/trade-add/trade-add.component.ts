import { Component, OnInit, Input } from '@angular/core';
import { RestApiService } from '../shared/rest-api.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-trade-add',
  templateUrl: './trade-add.component.html',
  styleUrls: ['./trade-add.component.css']
})
export class TradeAddComponent implements OnInit {

  @Input() tradeDetails = {  id: 0,
    stockTicker: '',
    price: 0,
    volume: 0,
    buyOrSell: '',
    orderTime: new Date(),
    statusCode: 0 }

  constructor(
    public restApi: RestApiService, 
    public router: Router
  ) { }

  ngOnInit(): void {
  }

  newTrade() {
    this.restApi.newTrade(this.tradeDetails).subscribe((data: {}) => {
      this.router.navigate(['/trade-list'])
    })
  }


}
