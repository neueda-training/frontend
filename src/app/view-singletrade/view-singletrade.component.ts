import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-view-singletrade',
  templateUrl: './view-singletrade.component.html',
  styleUrls: ['./view-singletrade.component.css']
})
export class ViewSingletradeComponent implements OnInit {
  id = this.actRoute.snapshot.params['id'];
  tradeDetails: any = {};
  constructor(
    public restApi: RestApiService,
    public actRoute: ActivatedRoute,
    public router: Router
  ) { }
  

  ngOnInit(): void {
    this.loadTrade()
  }

  loadTrade(){
    return this.restApi.getTrade(this.id).subscribe((data: {}) => {
      this.tradeDetails = data;
  })
  }
}
