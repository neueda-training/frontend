import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewSingletradeComponent } from './view-singletrade.component';

describe('ViewSingletradeComponent', () => {
  let component: ViewSingletradeComponent;
  let fixture: ComponentFixture<ViewSingletradeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewSingletradeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewSingletradeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
