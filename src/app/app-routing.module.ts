import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TradeAddComponent } from './trade-add/trade-add.component';
import { TradeEditComponent } from './trade-edit/trade-edit.component';
import { TradeListComponent } from './trade-list/trade-list.component';
import { ViewSingletradeComponent } from './view-singletrade/view-singletrade.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'trade-list' },
  { path: 'trade-add', component: TradeAddComponent },
  { path: 'trade-list', component: TradeListComponent},
  { path: 'trade-edit/:id', component: TradeEditComponent }  ,
  { path: 'trade-list/:id', component: ViewSingletradeComponent},
  { path: 'portfolio', component: PortfolioComponent} 
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
