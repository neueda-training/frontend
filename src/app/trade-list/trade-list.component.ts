import { Component, OnInit } from '@angular/core';
import { RestApiService } from '../shared/rest-api.service';
import { formatDate } from '@angular/common'

@Component({
  selector: 'app-trade-list',
  templateUrl: './trade-list.component.html',
  styleUrls: ['./trade-list.component.css']
})

export class TradeListComponent implements OnInit {
  Trades: any = [];
  constructor(public restApi: RestApiService) { }

  ngOnInit(): void {
    this.loadTrades()
  }

  loadTrades(){
    return this.restApi.getAllTrades().subscribe((data: {}) => {
      this.Trades = data;
  })
  }

  deleteTrade(id:any) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.restApi. deleteTrade(id).subscribe(data => {
        this.loadTrades()
      })
    }
  } 

  // getDate(orderTime:any){
  //   return formatDate(orderTime,'medium', 'en-GB');

  // }
  
  getStatusPhrase(statusCode:any){
    switch(statusCode) {
      case 0:
        return "Initial State"
        break;
      case 1:
        return "Processing"
        break;
      case 2:
          return "Success"
        break;
      default:
        return "Failed"
    }

}


}